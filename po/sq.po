# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Besnik Bleta <besnik@programeshqip.org>, 2007,2018,2020
# Besnik Bleta <besnik@programeshqip.org>, 2007,2021
# Besnik Bleta <besnik@programeshqip.org>, 2018
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-30 00:54+0100\n"
"PO-Revision-Date: 2022-01-29 23:54+0000\n"
"Last-Translator: Besnik Bleta <besnik@programeshqip.org>\n"
"Language-Team: Albanian (http://www.transifex.com/xfce/xfce-panel-plugins/language/sq/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sq\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../panel-plugin/cpu.c:63
msgid "File /proc/stat not found!"
msgstr "S’u gjet kartelë /proc/stat!"

#: ../panel-plugin/systemload.c:217
#, c-format
msgid "System Load: %ld%%"
msgstr "Ngarkesë Sistemi: %ld%%"

#: ../panel-plugin/systemload.c:225
#, c-format
msgid "Memory: %ldMB of %ldMB used"
msgstr "Kujtesë: e përdorur %ldMB nga %ldMB"

#: ../panel-plugin/systemload.c:233
#, c-format
msgid "Network: %ld Mbit/s"
msgstr "Rrjet: %ld Mbit/s"

#: ../panel-plugin/systemload.c:243
#, c-format
msgid "Swap: %ldMB of %ldMB used"
msgstr "\"Swap\": përdorur %ldMB nga %ldMB"

#: ../panel-plugin/systemload.c:246
#, c-format
msgid "No swap"
msgstr "Pa \"swap\""

#: ../panel-plugin/systemload.c:261
#, c-format
msgid "%dd"
msgstr "%dd"

#: ../panel-plugin/systemload.c:262
#, c-format
msgid "%dh"
msgstr "%dh"

#: ../panel-plugin/systemload.c:263
#, c-format
msgid "%dm"
msgstr "%dm"

#: ../panel-plugin/systemload.c:265
#, c-format
msgid "%d day"
msgid_plural "%d days"
msgstr[0] "%d ditë"
msgstr[1] "%d ditë"

#: ../panel-plugin/systemload.c:266
#, c-format
msgid "%d hour"
msgid_plural "%d hours"
msgstr[0] "%d orë"
msgstr[1] "%d orë"

#: ../panel-plugin/systemload.c:267
#, c-format
msgid "%d minute"
msgid_plural "%d minutes"
msgstr[0] "%d minutë"
msgstr[1] "%d minuta"

#: ../panel-plugin/systemload.c:274
#, c-format
msgid "Uptime: %s, %s, %s"
msgstr "Kohë funksionimi: %s, %s, %s"

#: ../panel-plugin/systemload.c:750
msgid "Label:"
msgstr "Etiketë:"

#: ../panel-plugin/systemload.c:759
msgid "Leave empty to disable the label"
msgstr "Që të çaktivizohet etiketa, lëreni të zbrazët"

#: ../panel-plugin/systemload.c:800
msgid "CPU monitor"
msgstr "Mbikëqyrës CPU-je"

#: ../panel-plugin/systemload.c:801
msgid "Memory monitor"
msgstr "Mbikëqyrës kujtese"

#: ../panel-plugin/systemload.c:802
msgid "Network monitor"
msgstr "Mbikëqyrës rrjeti"

#: ../panel-plugin/systemload.c:803
msgid "Swap monitor"
msgstr "Mbikëqyrës swap-i"

#: ../panel-plugin/systemload.c:804
msgid "Uptime monitor"
msgstr "Mbikëqyrës kohëpunimi"

#: ../panel-plugin/systemload.c:815 ../panel-plugin/systemload.desktop.in.h:1
msgid "System Load Monitor"
msgstr "Mbikëqyrës Ngarkese Sistemi"

#: ../panel-plugin/systemload.c:818
msgid "_Close"
msgstr "_Mbylle"

#: ../panel-plugin/systemload.c:819
msgid "_Help"
msgstr "_Ndihmë"

#: ../panel-plugin/systemload.c:837
msgid "<b>General</b>"
msgstr "<b>Të përgjithshme</b>"

#: ../panel-plugin/systemload.c:856
msgid "Update interval:"
msgstr "Interval përditësimi:"

#: ../panel-plugin/systemload.c:862
msgid ""
"Update interval when running on battery (uses regular update interval if set"
" to zero)"
msgstr "Interval përditësimi kur xhirohet me bateri (përdor interval të rregullt përditësime, nëse vlera caktohet zero)"

#: ../panel-plugin/systemload.c:873
msgid "Power-saving interval:"
msgstr "Interval kursimi energjie:"

#: ../panel-plugin/systemload.c:880
msgid "Launched when clicking on the plugin"
msgstr "E nisur kur klikohet mbi shtojcën"

#: ../panel-plugin/systemload.c:887
msgid "System monitor:"
msgstr "Mbikëqyrës sistemi:"

#: ../panel-plugin/systemload.c:925 ../panel-plugin/systemload.desktop.in.h:2
msgid "Monitor CPU load, swap usage and memory footprint"
msgstr "Mbikëqyr ngarkese CPU-je, përdorim hapësire swap-i dhe harxhim kujtese"

#: ../panel-plugin/systemload.c:927
msgid "Copyright (c) 2003-2022\n"
msgstr "Të drejta kopjimi (c) 2003-2022\n"

#: ../panel-plugin/uptime.c:52
msgid "File /proc/uptime not found!"
msgstr "S’u gjet kartelë /proc/uptime!"
